#include <stdio.h>
#include <stdlib.h>

int main()
{
    // Creamos una variable de tipo char
	char letra;

	// Pedimos un caracter al usuario
	printf("Ingresa una letra: ");

	// Guardamos el caracter en la variable letra
	scanf("%c", &letra);

	// Le mostramos la letra que ingreso
	printf("Ingresaste la letra %c", letra);

	// Creamos una variable de tipo int
	int numero;

	// Pedimos un numero entero al usuario
	printf("\nIngresa un numero entero: ");

	// Guardamos el numero en la variable numero
	scanf("%d", &numero);

	// Le mostramos el numero que ingreso
	printf("Ingresaste el numero %d", numero);

	// Creamos una variable de tipo short
	short numeroShort;

	// Pedimos un numero entero al usuario
	printf("\nIngresa un numero peque�o: ");

	// Guardamos el numero en la variable numeroShort
	scanf("%d", &numeroShort);

	// Le mostramos el numero que ingreso
	printf("Ingresaste el numero %d", numeroShort);

	// Creamos una variable de tipo float
	float numeroFloat;

	// Pedimos un numero con decimales al usuario
	printf("\nIngresa un numero con decimales: ");

	// Guardamos el numero en la variable numeroFloat
	scanf("%f", &numeroFloat);

	// Le mostramos el numero que ingreso
	printf("Ingresaste el numero %f", numeroFloat);
    return 0;
}
